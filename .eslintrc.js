module.exports = {
   'env': {
      'es6': true,
      'browser': true,
   },
   'extends': 'google',
   'rules': {
      'require-jsdoc': 0,
      'class-methods-use-this': 2,
      'no-invalid-this': 2,
      'no-shadow': 2,
   },
};
