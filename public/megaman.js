const TIPTOESPEED = 1;
/* Physics values are in pixels/second */
const HSPEED = 82.5;
const VSPEED = 285;
const GRAVITY = 650;
const FALLSPEED = 500;

const SHOT_COUNTER = 30;
const HIT_COUNTER = 30;
const TIPTOETIMER = 4;

let w = 17;
let h = 23;
let xOff = 12;
let yOff = 10;

const STANDING = 0;
const RUNNING = 1;
const JUMPING = 2;
const TIPTOE = 3;
const HIT = 4;

let land;

function MegaMan(game) {
   this.game = game;
   this.sprite = null;
}

MegaMan.prototype.preload = function() {
   game.load.spritesheet('megasheet', 'assets/megasheet5.png',
      41, 41);
   game.load.image('shot', 'assets/mega_shot.png');
   game.load.audio('land', 'assets/sfx/land.wav');
   game.load.audio('shoot', 'assets/sfx/mega_shoot.wav');
};

MegaMan.prototype.loadBullets = function() {
   this.bullets = game.add.group();
   this.bullets.enableBody = true;
   this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
   for (let i = 0; i < 3; i++) {
      let b = this.bullets.create(0, 0, 'shot');
      b.exists = false;
      b.visible = false;
      b.checkWorldBounds = true;
      b.events.onOutOfBounds.add( (bullet) => {
         bullet.kill();
      }, this);
   }
};

// MegaMan.prototype.create = () => {
MegaMan.prototype.create = function() {
   // Load sprites
   this.sprite = game.add.sprite(0, 0, 'megasheet');
   this.sprite.animations.add(
      'teleport', // name
      [0, 1, 2], // frames (Array)
      5, // frameRate
      true, // loop
      true // useNumericIndex (indexes or strings?)
   );
   let standArray = [3, 3, 3, 3, 3, 3, 3, 3, 3, 4];
   let runArray = [6, 7, 8, 7];
   let runFrameRate = 7;
   this.sprite.animations.add('stand', standArray, 5, true, 2);
   this.sprite.animations.add('run', runArray, runFrameRate, true, true);
   this.sprite.animations.add('tiptoe', [5], 5, true, true);
   this.sprite.animations.add('jump', [9], 5, true, 2);

   this.sprite.animations.add('stand_shoot', [11], 5, true, 2);
   let runShootArray = [12, 13, 14, 13];
   this.sprite.animations.add('run_shoot', runShootArray, runFrameRate, true, true);
   this.sprite.animations.add('tiptoe_shoot', [11], 5, true, true);
   this.sprite.animations.add('jump_shoot', [10], 5, true, 2);
   this.sprite.animations.add('hit', [15], 5, true, 2);

   // Set first animation
   this.sprite.animations.play('stand');

   // Load sounds
   land = game.add.audio('land');
   land.volume -= .8;
   shoot = game.add.audio('shoot');
   shoot.volume -= .8;

   // Load Physics
   game.physics.enable(this.sprite);
   this.sprite.body.setSize(w, h, xOff, yOff);
   this.sprite.body.gravity.y = GRAVITY;
   this.sprite.anchor.setTo(0.5, 0.5);
   this.sprite.body.collideWorldBounds = true;

   // Load own variables
   this.loadBullets();

   this.megaState = JUMPING;
   this.tiptoeCounter = 0;
   this.shootCounter = 0; // for animation
   this.hitCounter = 0;
   this.isShooting = false;
   this.facing = 1;
   this.oldInputs = {
      up: false,
      down: false,
      left: false,
      right: false,
      shoot: false,
      jump: false,
   };
};

MegaMan.prototype._setAnimationFromState = function() {
   if (this.megaState === HIT) {
      this.sprite.animations.play('hit');
      return;
   }

   if (this.isShooting) {
      switch(this.megaState) {
         case STANDING:
            this.sprite.animations.play('stand_shoot');
            break;
         case JUMPING:
            this.sprite.animations.play('jump_shoot');
            break;
         case RUNNING:
            // TODO
            let oldName = this.sprite.animations.name;
            let oldAnim = this.sprite.animations.currentAnim;
            let oldIndex = oldAnim._frameIndex;
            this.sprite.animations.play('run_shoot');
            if (oldName === 'run') {
               this.sprite.animations.currentAnim.setFrame(oldIndex, true);
               // sprite.animations.next();
            }
            break;
         case TIPTOE:
            this.sprite.animations.play('tiptoe_shoot');
            break;
         default:
            // fuk
            break;
      }
   } else {
      switch(this.megaState) {
         case STANDING:
            this.sprite.animations.play('stand');
            break;
         case JUMPING:
            this.sprite.animations.play('jump');
            break;
         case RUNNING:
            let oldName = this.sprite.animations.name;
            let oldAnim = this.sprite.animations.currentAnim;
            let oldIndex = oldAnim._frameIndex;
            this.sprite.animations.play('run');
            if (oldName === 'run_shoot') {
               this.sprite.animations.currentAnim.setFrame(oldIndex, true);
               // sprite.animations.next();
            }
            break;
         case TIPTOE:
            this.sprite.animations.play('tiptoe');
            break;
         default:
            // fuk
            break;
      }
   }
};

MegaMan.prototype._setStateForLeftAndRight = function(inputs) {
   let isLeft = inputs.left;
   let isRight = inputs.right;
   if (isLeft || isRight) {
      if (this.megaState === STANDING) {
         this.megaState = TIPTOE;
         this.tiptoeCounter = TIPTOETIMER;
      } else if (this.megaState === TIPTOE) {
         if (this.tiptoeCounter > 0) {
            this.tiptoeCounter -= 1;
         } else {
            this.megaState = RUNNING;
         }
      }
   } else {
      if (this.megaState === RUNNING) {
         this.megaState = TIPTOE;
         this.tiptoeCounter = TIPTOETIMER;
      } else if (this.megaState === TIPTOE) {
         if (this.tiptoeCounter > 0) {
            this.tiptoeCounter -= 1;
         } else {
            this.megaState = STANDING;
         }
      }
   }
};

MegaMan.prototype._setXSpeed = function(inputs) {
   let velocity = this.sprite.body.velocity;
   let isLeft = inputs.left;
   let isRight = inputs.right;
   if (this.megaState === RUNNING) {
      if (isLeft) velocity.x = -HSPEED;
      else if (isRight) velocity.x = HSPEED;
   } else if (this.megaState === JUMPING) {
      if (isLeft) velocity.x = -HSPEED;
      else if (isRight) velocity.x = HSPEED;
      else velocity.x = 0;
   } else if (this.megaState === TIPTOE) {
      if (isLeft) velocity.x = -TIPTOESPEED;
      else if (isRight) velocity.x = TIPTOESPEED;
   } else if (this.megaState === STANDING) {
      velocity.x = 0;
   }
};

MegaMan.prototype._handleJumping = function(inputs) {
   let velocity = this.sprite.body.velocity;
   if (velocity.y > 0) { // Is falling
      if (velocity.y > FALLSPEED) { // Is falling
         velocity.y = FALLSPEED;
      }
   } else if (velocity.y < 0) { // Is going up
      if (!inputs.jump) {
         velocity.y = 0;
      }
   }

   if (this.sprite.body.blocked.down) {
      land.play();
      if (Math.abs(velocity.x) > 0) this.megaState = RUNNING;
      else this.megaState = STANDING;
   }
};

// Returns true if the bullet came out
MegaMan.prototype._fireBullet = function() {
   let bullet = this.bullets.getFirstExists(false);
   if (bullet) {
      shoot.play();
      let xOffset = 20 * this.facing;
      let yOffset = -1;
      bullet.reset(this.sprite.x + xOffset, this.sprite.y + yOffset);
      bullet.body.velocity.x = 300 * this.facing;
      return true;
   }
};

// Recycle bullets if they go out of range
MegaMan.prototype._cleanBullets = function() {
   this.bullets.forEach( (child) => {
      let bDistance = this.sprite.x - child.x;
      let limit = game.width - 30;
      if (bDistance > limit || bDistance < - limit) {
         child.kill();
      }
   }, this);
};

let _handleEnemyCollision = function(meg, enemy) {
   // TODO global hack
   if (megaman.megaState !== HIT) {
      megaman.megaState = HIT;
      megaman.hitCounter = HIT_COUNTER;
      megaman.sprite.body.velocity.x = megaman.facing * -40;
      megaman.sprite.body.velocity.y = 0;
      // TODO lower hp
   }
};

MegaMan.prototype.update = function(cl, newInputs, enemies) {
   let oldMegaState = this.megaState;
   let oldShooting = this.isShooting;
   let velocity = this.sprite.body.velocity;

   game.physics.arcade.collide(this.sprite, cl);
   game.physics.arcade.collide(this.sprite, enemies, _handleEnemyCollision);

   if (this.megaState !== HIT) {
      this._setStateForLeftAndRight(newInputs);
      this._setXSpeed(newInputs);

      // Jumping stuff
      if (this.megaState === STANDING ||
         this.megaState === TIPTOE ||
         this.megaState === RUNNING) {
         if (newInputs.jump) {
            velocity.y = -VSPEED;
            this.megaState = JUMPING;
         }
         if (velocity.y > 0) { // Is falling
            this.megaState = JUMPING;
         }
      } else if (this.megaState === JUMPING) {
         this._handleJumping(newInputs);
      }

      // Shooting stuff
      this._cleanBullets();

      if (!this.oldInputs.shoot && newInputs.shoot) {
         this.isShooting = this._fireBullet();
         this.shootCounter = SHOT_COUNTER;
      } else if (this.shootCounter > 0) {
         this.shootCounter--;
      } else {
         this.isShooting = false;
      }
   } else if (this.megaState === HIT) {
      this.hitCounter -= 1;
      if (this.hitCounter < 0) {
         this.megaState = STANDING;
      }
   }

   // Wrap Y position
   if (this.sprite.body.y > 256) this.sprite.body.y = 0;

   if (oldMegaState !== this.megaState || oldShooting !== this.isShooting) {
      this._setAnimationFromState();
   }

   // Flip sprite
   if (this.sprite.body.velocity.x > 0) {
      this.sprite.scale.x = Math.abs(this.sprite.scale.x);
      this.facing = 1;
      this.sprite.body.setSize(w, h, xOff, yOff);
   } else if (this.sprite.body.velocity.x < 0) {
      this.sprite.scale.x = Math.abs(this.sprite.scale.x) * -1;
      this.facing = -1;
      this.sprite.body.setSize(w, h, xOff + 1, yOff);
   }

   this.oldInputs = newInputs;
};

MegaMan.prototype.render = function() {
   game.debug.bodyInfo(this.sprite, 16, 16);
   game.debug.body(this.sprite);
};

MegaMan.prototype.getX = function() {
   return this.sprite.body.x;
};

MegaMan.prototype.getXVelo = function() {
   return this.sprite.body.velocity.x;
};

MegaMan.prototype.getBullets = function() {
   return this.bullets;
};
