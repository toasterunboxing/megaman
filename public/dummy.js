function Dummy(game, x, y) {
   Phaser.Sprite.call(this, game, x, y, 'dummy');
   this.game = game;
   game.physics.enable(this);
   this.hit = game.add.audio('enemyHit');
   this.hit.volume -= .5;
}
Dummy.prototype = Object.create(Phaser.Sprite.prototype);
Dummy.prototype.constructor = Dummy;

Dummy.prototype.preload = function() {
};

Dummy.prototype.update = function() {
};

// Called by bullets on collision
Dummy.prototype.die = function() {
   this.hit.play();
   // TODO create the explosion object
   this.kill();
};

Dummy.prototype.render = function() {
   game.debug.body(this);
};
