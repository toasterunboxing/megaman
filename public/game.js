const TILE_LENGTH = 16;
const TILE_WIDTH = 16;
const DEBUG = false;
const SCALE = 3;

let game = new Phaser.Game(
   256, 240,
   // 1000, 240,
   Phaser.CANVAS,
   '',
   {
      init: init,
      preload: preload,
      create: create,
      render: render,
      update: update,
   }
   // false, // transparent
   // false // antialias
);

let input;
let megaman;
let enemies;

let map;
let collisionLayer;
let bgLayer;
let animatedTiles;

function init() {
}

function preload() {
   // Music
   game.load.audio('metal_music', 'assets/music/metalman_stage.ogg');
   game.load.audio('wood_music', 'assets/music/woodman_stage.ogg');
   game.load.audio('bubble_music', 'assets/music/bubbleman_stage.ogg');

   // Pixel perfect
   game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
   game.scale.setUserScale(SCALE, SCALE);
   game.renderer.renderSession.roundPixels = true;
   Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
   // Phaser game stuff
   game.scale.parentIsWindow = true;
   game.stage.backgroundColor = '#ff69b4';
   // Input stuff
   input = new Input(game);
// Mega Man
   megaman = new MegaMan(game);
   megaman.preload();
   // Enemies
   game.load.image('dummy', 'assets/dummy.png');
   game.load.spritesheet('joker', 'assets/joker.png',
      32, 32);
   game.load.audio('enemyHit', 'assets/sfx/enemy_hit.wav');
   // The map
   game.load.tilemap('map',
      'assets/tilemaps/test2.json',
      null,
      Phaser.Tilemap.TILED_JSON);
   game.load.image('metalman', 'assets/tilemaps/metaltileset.png');
   game.load.spritesheet('metalsheet', 'assets/tilemaps/metaltileset.png',
         TILE_LENGTH, TILE_WIDTH, -1, 0, 1);
}

function animateTile(tile, index1, index2) {
   let ani = animatedTiles.create(tile.x * 16, tile.y * 16, 'metalsheet');
   ani.animations.add('b', [index1, index2], 5, true, true);
   ani.play('b');
}

function getRandomInt(min, max) {
     min = Math.ceil(min);
     max = Math.floor(max);
     return Math.floor(Math.random() * (max - min)) + min;
}

function create() {
   let musicArray = ['metal_music', 'wood_music', 'wood_music'];
   let music = game.add.audio(musicArray[getRandomInt(0, musicArray.length-1)]);
   music.volume -= 0.9;
   music.loopFull();
   input.create();
   // The map
   map = game.add.tilemap('map');
   map.addTilesetImage('metalman', 'metalman');
   collisionLayer = map.createLayer('collisionLayer');
   collisionLayer.resizeWorld();
   bgLayer = map.createLayer('bg');
   // Animated Tiles
   animatedTiles = game.add.group();
   let tiles = bgLayer.getTiles(0, 0, bgLayer.width * 16, bgLayer.height * 16);
   for (let tile of tiles) {
      if (tile.index === 41) {
         animateTile(tile, 40, 40 + 11);
      } else if (tile.index === 31) {
         animateTile(tile, 30, 30 + 22);
      } else if (tile.index === 32) {
         animateTile(tile, 31, 31 + 22);
      } else if (tile.index === 20) {
         animateTile(tile, 19, 19 + 22);
      } else if (tile.index === 21) {
         animateTile(tile, 20, 20 + 22);
      }
   }

   // All nonzero tiles are collision tiles
   map.setCollisionBetween(1, 10000, true, collisionLayer);
   megaman.create();
   // Enemy stuff
   enemies = game.add.physicsGroup();
   enemies.classType = Joker;
   for (let i = 0; i < 1; i++) {
      enemies.create(64, 64);
   }
   game.camera.follow(megaman.sprite);
   let foo = game.width * 0.25;
   game.camera.deadzone =
      new Phaser.Rectangle(foo, 70, game.width - 2 * foo, game.height);
}

function handleBulletEnemyCollision(bullet, enemy) {
   bullet.kill();
   enemy.die();
}

function update() {
   megaman.update(collisionLayer, input.getInputs(), enemies);
   game.physics.arcade.collide(megaman.getBullets(), enemies, handleBulletEnemyCollision);
   game.physics.arcade.collide(collisionLayer, enemies);
   // let megaX = megaman.getX();
   // let camBorder = 70;
   // let cameX = game.camera.x;
   // let camDiffX = megaX - game.camera.x;
   // let camSpeed = megaman.sprite.body.deltaX();
   // if (cameX + game.width - megaX - megaman.sprite.body.width < camBorder && camSpeed > 0) {
   //    game.camera.setPosition(cameX + 1, 0);
   // } else if (camDiffX < camBorder && camSpeed < 0) {
   //    game.camera.setPosition(cameX - 1, 0);
   // }
}

function render() {
   if (DEBUG) {
      zone = game.camera.deadzone;
      game.context.fillRect(zone.x, zone.y, zone.width, zone.height);
      game.time.advancedTiming = true;
      game.debug.text(game.time.fps || '--', 2, 14, '#00ff00');
      megaman.render();
      enemies.forEach((e) => e.render());
   }
}
