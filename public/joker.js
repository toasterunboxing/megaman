const WAITING = 0;
const FALLING = 1;
const GOING = 2;

function Joker(game, x, y) {
   Phaser.Sprite.call(this, game, x, y, 'joker');
   this.game = game;
   game.physics.enable(this);
   this.hit = game.add.audio('enemyHit');
   this.hit.volume -= .5;

   // Sprite stuff
   this.animations.add('stand', [0, 1], 5, true, true);
   this.animations.add('gear', [2, 3], 5, true, true);

   this.animations.play('gear');
   this.state = WAITING;
}
Joker.prototype = Object.create(Phaser.Sprite.prototype);
Joker.prototype.constructor = Joker;

Joker.prototype.preload = function() {
};

Joker.prototype.update = function(cl) {
   if (this.state === WAITING) {
      // TODO if MegaMan gets in range, switch to falling
      this.state = FALLING;
   } else if (this.state === FALLING) {
      this.body.velocity.y = 100;
      if (this.body.blocked.down) {
         this.state = GOING;
         this.body.velocity.x = -100;
      }
      // When on the ground, switch to running
   } else if (this.state === GOING) {
      if (this.body.blocked.right) {
         this.body.velocity.x = -100;
      } else if (this.body.blocked.left) {
         this.body.velocity.x = 100;
      }
      // go go go!
   }
};

// Called by bullets on collision
Joker.prototype.die = function() {
   this.hit.play();
   // TODO create the explosion object
   this.kill();
};

Joker.prototype.render = function() {
   game.debug.body(this);
};
