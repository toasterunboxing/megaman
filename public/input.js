const K_UP = Phaser.Keyboard.W;
const K_DOWN = Phaser.Keyboard.S;
const K_LEFT = Phaser.Keyboard.A;
const K_RIGHT = Phaser.Keyboard.D;
const K_SHOOT = Phaser.Keyboard.J;
const K_JUMP = Phaser.Keyboard.K;

const J_DPAD_UP = Phaser.Gamepad.XBOX360_DPAD_UP;
const J_DPAD_DOWN = Phaser.Gamepad.XBOX360_DPAD_DOWN;
const J_DPAD_LEFT = Phaser.Gamepad.XBOX360_DPAD_LEFT;
const J_DPAD_RIGHT = Phaser.Gamepad.XBOX360_DPAD_RIGHT;
const J_SHOOT = Phaser.Gamepad.XBOX360_A;
const J_JUMP = Phaser.Gamepad.XBOX360_B;

function Input(game) {
}

Input.prototype.create = function() {
   game.input.gamepad.start();
};

// Returns an object which contains true/false values of inputs
Input.prototype.getInputs = function() {
   let keyboard = game.input.keyboard;
   let pad = game.input.gamepad;

   let up = keyboard.isDown(K_UP) || pad.isDown(J_DPAD_UP);
   let down = keyboard.isDown(K_DOWN) || pad.isDown(J_DPAD_DOWN);
   let left = keyboard.isDown(K_LEFT) || pad.isDown(J_DPAD_LEFT);
   let right = keyboard.isDown(K_RIGHT) || pad.isDown(J_DPAD_RIGHT);
   let shoot = keyboard.isDown(K_SHOOT) || pad.isDown(J_SHOOT);
   let jump = keyboard.isDown(K_JUMP) || pad.isDown(J_JUMP);

   let newInputs = {
      up: up,
      down: down,
      left: left,
      right: right,
      shoot: shoot,
      jump: jump,
   };
   return newInputs;
};
